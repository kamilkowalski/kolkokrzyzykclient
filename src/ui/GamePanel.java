package ui;

import models.GamePanelModel;
import models.GameListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GamePanel extends JPanel implements ActionListener, GameListener {

    private JButton[][] fields = new JButton[3][3];
    private GamePanelModel model;

    public enum PlayerSign {
        X, O, EMPTY
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        for(int x=0; x<fields.length; x++){
            for(int y=0; y<fields[x].length; y++){
                if(fields[x][y] == (JButton)e.getSource()){
                    model.setValue(model.getMySign(), x, y);
                }
            }
        }
    }

    @Override
    public void valueChanged(PlayerSign[][] values, boolean enabled, GamePanelModel.GameFinishedStatus status) {
        for(int x=0; x<values.length; x++){
            for(int y=0; y<values[x].length; y++){

                JButton field = fields[x][y];
                PlayerSign value = values[x][y];

                if(value == PlayerSign.EMPTY){
                    field.setText("");
                    fields[x][y].setEnabled(enabled);
                } else {
                    field.setText(value.toString());
                    fields[x][y].setEnabled(false);
                }

                if(status == GamePanelModel.GameFinishedStatus.ME){
                    field.setForeground(Color.GREEN);
                } else if(status == GamePanelModel.GameFinishedStatus.HIM){
                    field.setForeground(Color.RED);
                }
            }
        }
    }

    public GamePanel(GamePanelModel m){
        setBorder(BorderFactory.createTitledBorder("Gra"));
        setLayout(new GridLayout(3, 3));

        // Pola gry
        for(int x=0; x<3; x++){
            for(int y=0; y<3; y++){
                fields[x][y] = new JButton();
                fields[x][y].setContentAreaFilled(false);
                fields[x][y].addActionListener(this);
                add(fields[x][y]);
            }
        }

        model = m;
        model.addChangeListener(this);
    }
}
