package models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChatMessage {

    private String message;
    private int author;
    private Date createdAt;

    public ChatMessage(String msg, int aut, Date cr){
        message = msg;
        author = aut;
        createdAt = cr;
    }

    public static ChatMessage parse(String msg){

        int auth = msg.charAt(0) - 48;
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        Date d;

        try {
            d = df.parse(msg.substring(2, 20));
        } catch(ParseException e){
            d = new Date();
        }

        return new ChatMessage(msg.substring(22), auth, d);
    }

    public void setMessage(String m){
        message = m;
    }

    public String getMessage(){
        return message;
    }

    public void setAuthor(int a){
        author = a;
    }

    public int getAuthor(){
        return author;
    }

    public void setCreatedAt(Date cr){
        createdAt = cr;
    }

    public Date getCreatedAt(){
        return createdAt;
    }
}
