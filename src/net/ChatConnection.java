package net;

import models.ChatMessage;
import models.ChatPanelModel;
import models.GamePanelModel;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ChatConnection implements Runnable {

    private ChatPanelModel model;
    private GamePanelModel gameModel;
    private String address;
    private Socket connection;
    private DataOutputStream output;
    private BufferedReader reader;
    private boolean connected = false;

    public ChatConnection(ChatPanelModel m, GamePanelModel gm, String addr){
        model = m;
        address = addr;
        gameModel = gm;

        try {
            connection = new Socket(address, 8060);
            output = new DataOutputStream(connection.getOutputStream());
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        } catch(IOException e){
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        try {
            while(true){
                if(connected) {
                    String line = reader.readLine();
                    System.out.println("Read line: " + line);
                    ChatMessage chm = ChatMessage.parse(line);

                    model.addMessage(chm);
                } else {
                    if(gameModel.getGameId() > 0){
                        output.writeBytes("" + gameModel.getGameId() + "\n");
                        connected = true;
                        model.setEnabled(true);
                    }
                }

                try {
                    Thread.sleep(50);
                } catch(InterruptedException e){
                    e.printStackTrace();
                }
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    public DataOutputStream getOutput(){
        return output;
    }
}
