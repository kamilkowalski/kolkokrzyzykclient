package net;

import models.GamePanelModel;
import screens.GameScreen;
import ui.GamePanel;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class GameConnection implements Runnable {

    private GamePanelModel model;
    private GameScreen screen;

    private String address;
    private Socket sock;
    private BufferedReader reader;
    private DataOutputStream output;

    private ConnectionState state;

    public enum ConnectionState {
        INIT, MOVE, UPDATE, WAITING, FINISHED
    }

    public GameConnection(GameScreen s, GamePanelModel m, String addr){
        model = m;
        screen = s;
        address = addr;

        try {
            sock = new Socket(address, 8050);
            output = new DataOutputStream(sock.getOutputStream());
            reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    public DataOutputStream getOutput(){
        return output;
    }

    @Override
    public void run() {
        try {
            state = ConnectionState.INIT;

            while(true){
                switch(state){
                    case INIT:
                        model.setEnabled(false);

                        String sign = reader.readLine();

                        System.out.println("Sign assigned to client: " + sign);

                        if(sign.equals("X")){
                            model.setMySign(GamePanel.PlayerSign.X);
                        } else if(sign.equals("O")){
                            model.setMySign(GamePanel.PlayerSign.O);
                        }

                        int gameId = Integer.parseInt(reader.readLine());

                        System.out.println("Game ID: " + gameId);

                        model.setGameId(gameId);

                        state = ConnectionState.WAITING;

                    case WAITING:
                        model.setEnabled(false);

                        String command = reader.readLine();

                        System.out.println("Received command: " + command);

                        if(command != null) {
                            if (command.equals("MV")) {
                                state = ConnectionState.MOVE;
                            } else if (command.equals("UPD")) {
                                state = ConnectionState.UPDATE;
                            } else if (command.equals("YOU_WIN")) {
                                // handle win
                                model.setGameFinished(GamePanelModel.GameFinishedStatus.ME);
                            } else if (command.equals("YOU_LOSE")) {
                                // handle loss
                                model.setGameFinished(GamePanelModel.GameFinishedStatus.HIM);
                            }
                        } else {
                            state = ConnectionState.FINISHED;
                        }
                        break;
                    case UPDATE:
                        int x = Integer.parseInt(reader.readLine());
                        int y = Integer.parseInt(reader.readLine());

                        System.out.println("Received coords: " + x + ", " + y);

                        model.setValue(model.getHisSign(), x, y);
                        state = ConnectionState.WAITING;

                        output.writeBytes("OK\n");
                        break;
                    case MOVE:
                        model.setEnabled(true);
                        int[] userInput = model.getUserInput();

                        System.out.println("Sending move: " + userInput[0] + ", " + userInput[1]);

                        output.writeBytes("MV\n");
                        output.writeBytes("" + userInput[0] + "\n");
                        output.writeBytes("" + userInput[1] + "\n");

                        String response = reader.readLine();

                        if(response.equals("MV_VALID")){
                            state = ConnectionState.WAITING;
                        } else {
                            state = ConnectionState.MOVE;
                        }
                }
            }

        } catch(IOException e){
            e.printStackTrace();
        }
    }
}
