package screens;

import main.KolkoKrzyzykClient;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StartScreen extends JPanel implements ActionListener {

    private JButton connectButton;
    private JTextField serverAddress;
    private JTextField userName;
    private KolkoKrzyzykClient client;

    public StartScreen(KolkoKrzyzykClient c){
        super();

        client = c;
        connectButton = new JButton("Connect");
        serverAddress = new JTextField("localhost");
        userName = new JTextField("");
        serverAddress.setColumns(10);

        setPreferredSize(new Dimension(250, 100));
        setLayout(new BorderLayout());

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2, 2));

        add(serverAddress, BorderLayout.NORTH);
        add(connectButton, BorderLayout.CENTER);

        connectButton.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        client.startGame(serverAddress.getText());
    }
}
