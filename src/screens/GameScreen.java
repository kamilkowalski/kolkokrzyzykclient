package screens;

import models.ChatPanelModel;
import models.GamePanelModel;
import net.ChatConnection;
import net.GameConnection;
import ui.ChatPanel;
import ui.GamePanel;

import javax.swing.*;
import java.awt.*;

public class GameScreen extends JPanel {

    private ChatPanel chatPanel;
    private GamePanel gamePanel;
    private GameConnection gameConnection;
    private GamePanelModel gamePanelModel;
    private ChatPanelModel chatPanelModel;
    private ChatConnection chatConnection;

    public GameScreen(String addr){
        gamePanelModel = new GamePanelModel();
        gameConnection = new GameConnection(this, gamePanelModel, addr);

        // Game connection thread
        Thread connectionThread = new Thread(gameConnection);
        connectionThread.start();

        chatPanelModel = new ChatPanelModel();
        chatConnection = new ChatConnection(chatPanelModel, gamePanelModel, addr);

        // Chat connection thread
        Thread chatThread = new Thread(chatConnection);
        chatThread.start();

        chatPanel = new ChatPanel(chatPanelModel, gamePanelModel, chatConnection);
        gamePanel = new GamePanel(gamePanelModel);

        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        setPreferredSize(new Dimension(700, 200));

        add(gamePanel);
        add(chatPanel);
    }
}
