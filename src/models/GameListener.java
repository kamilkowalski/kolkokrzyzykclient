package models;

import ui.GamePanel;

public interface GameListener {
    public void valueChanged(GamePanel.PlayerSign[][] values, boolean enabled, GamePanelModel.GameFinishedStatus gameFinished);
}
