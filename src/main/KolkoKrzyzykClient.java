package main;

import screens.GameScreen;
import screens.StartScreen;

import javax.swing.*;
import java.awt.*;

public class KolkoKrzyzykClient extends JFrame {
    public KolkoKrzyzykClient(){
        Container c = getContentPane();
        StartScreen startScreen = new StartScreen(this);

        c.add(startScreen);
        pack();
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    public void startGame(String address){
        Container c = getContentPane();
        GameScreen gs = new GameScreen(address);

        c.removeAll();
        c.add(gs);

        pack();
    }

    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new KolkoKrzyzykClient();
            }
        });
    }
}
