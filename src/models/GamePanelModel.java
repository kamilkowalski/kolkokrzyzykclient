package models;

import ui.GamePanel;

import java.util.HashSet;
import java.util.Set;

public class GamePanelModel {

    private boolean enabled = false;
    private GamePanel.PlayerSign[][] values = new GamePanel.PlayerSign[3][3];
    private Set<GameListener> listeners = new HashSet<GameListener>();
    private GamePanel.PlayerSign mySign;
    private GamePanel.PlayerSign hisSign;
    private int gameId = 0;
    private GameFinishedStatus gameFinished;

    private int[] lastSelected = null;

    public enum GameFinishedStatus {
        NO, ME, HIM
    };

    public GamePanelModel(){
        resetValues();
    }

    public void addChangeListener(GameListener listener){
        listeners.add(listener);
        fireChangeEvent();
    }

    public synchronized void setValue(GamePanel.PlayerSign sign, int x, int y){
        values[x][y] = sign;

        if(sign == mySign) {
            lastSelected = new int[]{x, y};
        }

        fireChangeEvent();
    }

    public synchronized void setEnabled(boolean v){
        enabled = v;
        fireChangeEvent();
    }

    public synchronized boolean isEnabled(){
        return enabled;
    }

    public GamePanel.PlayerSign[][] getValues(){
        return values;
    }

    public void setMySign(GamePanel.PlayerSign sign){
        mySign = sign;

        if(sign == GamePanel.PlayerSign.X){
            hisSign = GamePanel.PlayerSign.O;
        } else {
            hisSign = GamePanel.PlayerSign.X;
        }
    }

    public GamePanel.PlayerSign getMySign(){
        return mySign;
    }

    public GamePanel.PlayerSign getHisSign(){
        return hisSign;
    }

    public void setGameId(int id){
        gameId = id;
        fireChangeEvent();
    }

    public int getGameId(){
        return gameId;
    }

    public int[] getUserInput(){
        while(true){
            if(lastSelected != null){
                int[] tmp = lastSelected;
                lastSelected = null;

                return tmp;
            }

            try {
                Thread.sleep(50);
            } catch(InterruptedException e){}
        }
    }

    public void setGameFinished(GameFinishedStatus status){
        gameFinished = status;
        fireChangeEvent();
    }

    private void resetValues(){
        lastSelected = null;
        enabled = false;
        gameFinished = GameFinishedStatus.NO;

        for(int x=0; x<values.length; x++){
            for(int y=0; y<values[x].length; y++){
                values[x][y] = GamePanel.PlayerSign.EMPTY;
            }
        }
    }

    private void fireChangeEvent(){
        for(GameListener l : listeners){
            l.valueChanged(values, enabled, gameFinished);
        }
    }
}
