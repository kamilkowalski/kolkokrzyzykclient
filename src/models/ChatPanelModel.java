package models;

import java.util.*;

public class ChatPanelModel {

    private boolean enabled;
    private List<ChatMessage> messages = new ArrayList<ChatMessage>();
    private LinkedList<ChatMessage> messageQueue = new LinkedList<ChatMessage>();
    private Set<ChatListener> listeners = new HashSet<ChatListener>();

    public void addChatListener(ChatListener listener){
        listeners.add(listener);
        fireChangeEvent();
    }


    public void setEnabled(boolean en){
        enabled = en;
        fireChangeEvent();
    }

    public boolean getEnabled(){
        return enabled;
    }

    public ChatMessage getMessageFromQueue(){
        synchronized(messageQueue) {
            if (messageQueue.size() > 0) {
                return messageQueue.remove();
            } else {
                return null;
            }
        }
    }

    public void addMessageToQueue(ChatMessage chm){
        synchronized(messageQueue){
            messageQueue.add(chm);
        }
    }

    public synchronized void addMessage(ChatMessage msg){
        messages.add(msg);
        fireChangeEvent();
    }

    private void fireChangeEvent(){
        for(ChatListener l : listeners){
            l.valueChanged(messages, enabled);
        }
    }


}
