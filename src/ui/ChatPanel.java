package ui;

import models.*;
import net.ChatConnection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;

public class ChatPanel extends JPanel implements ChatListener, GameListener, ActionListener {

    private ChatPanelModel model;
    private GamePanelModel gameModel;
    private JScrollPane scrollPane;
    private JTextArea chat;
    private JTextField message;
    private ChatConnection connection;

    public ChatPanel(ChatPanelModel m, GamePanelModel gpm, ChatConnection chc){
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createTitledBorder("Chat"));

        chat = new JTextArea();
        message = new JTextField();

        scrollPane = new JScrollPane(chat, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

        message.addActionListener(this);

        model = m;
        model.addChatListener(this);

        gameModel = gpm;
        gameModel.addChangeListener(this);

        connection = chc;

        add(scrollPane, BorderLayout.CENTER);
        add(message, BorderLayout.SOUTH);
    }

    @Override
    public void valueChanged(List<ChatMessage> messages, boolean enabled){
        chat.setText("");


        for (ChatMessage m : messages) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            chat.append("Player #" + m.getAuthor());
            chat.append(" [" + sdf.format(m.getCreatedAt()) + "]: ");
            chat.append(m.getMessage() + "\n");
        }

        chat.setEnabled(enabled);
        message.setEnabled(enabled);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(!message.getText().isEmpty()) {
            DataOutputStream output = connection.getOutput();

            try {
                output.writeBytes(message.getText() + "\n");
            } catch(IOException exc){
                exc.printStackTrace();
            }

            System.out.println("Sending out text: " + message.getText());
            message.setText("");
        }
    }

    @Override
    public void valueChanged(GamePanel.PlayerSign[][] values, boolean enabled, GamePanelModel.GameFinishedStatus gameFinished) {
        if (gameFinished == GamePanelModel.GameFinishedStatus.ME) {
            chat.append("You win!\n");
        } else if (gameFinished == GamePanelModel.GameFinishedStatus.HIM) {
            chat.append("You lose!\n");
        }
    }
}
