package models;

import java.util.List;

public interface ChatListener {
    public void valueChanged(List<ChatMessage> messages, boolean enabled);
}
